module Control.Functor.Elastic
  ( module Control.Functor.Expansive
  , module Control.Functor.Compactable
  , Elastic
  ) where


import           Control.Functor.Compactable (Compactable)
import           Control.Functor.Expansive   (Expansive)
import           Data.IntMap                 (IntMap)
import           Data.Map                    (Map)


class (Compactable f, Expansive f) => Elastic f


instance Elastic Maybe
instance Elastic IntMap
instance Ord k => Elastic (Map k)

