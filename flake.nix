{
  description = "Compactable";

  inputs = rec
    { x2009.url    = "github:nixos/nixpkgs/release-20.09";
      x2105.url    = "github:nixos/nixpkgs/release-21.05";
      x2205.url    = "github:nixos/nixpkgs/release-22.05";
      easy-hls.url = "github:jkachmar/easy-hls-nix";
      # easy-hls.url = "github:ursi/easy-hls-nix";
      # easy-hls.inputs.nixpkgs.follows = x2105;
      utils.url    = "github:numtide/flake-utils";
    };

  outputs = { x2009, x2105, x2205, easy-hls, utils, ... }:
    utils.lib.eachDefaultSystem
      (system:
        let
           withSystem = args: import ./default.nix ({ inherit system; } // args);
           inherit ((import x2205 { inherit system; }).haskell.lib) dontCheck;
        in rec {

           defaultPackage = packages.ghc922;

           devShell = withSystem
             { nixpkgs  = x2105;
               # easy-hls = easy-hls.withGhcs.${system} [ "8.10.4" ];
               easy-hls = easy-hls.defaultPackage.${system};
               compiler = "ghc8104";
               asShell  = true;
               withHoogle = true;
             };

           packages =
             (builtins.foldl' (acc: x: acc // {
               ${x} = withSystem { nixpkgs = x2009; compiler = "${x}"; };
             }) {} [ "ghc884" "ghc865" ]) //
             (builtins.foldl' (acc: x: acc // {
               ${x} = withSystem { nixpkgs = x2105; compiler = "${x}"; };
             }) {} [ "ghc901" "ghc8107" ]) //
             (builtins.foldl' (acc: x: acc // {
               ${x} = dontCheck (withSystem { nixpkgs = x2205; compiler = "${x}"; });
             }) {} [ "ghc922" ]);
         }
      );
}
