{ nixpkgs
, easy-hls ? false
, compiler ? "ghc8104"
, withHoogle ? false
, doHoogle ? false
, doHaddock ? false
, enableLibraryProfiling ? false
, enableExecutableProfiling ? false
, strictDeps ? false
, isJS ? false
, asShell ? false
, system ? builtins.currentSystem
, optimize ? true
}:
let


  ignorance = [
    "*.md"
    "*.adoc"
    "intro"
    "*.nix"
    "*.sh"
    "*.yml"
  ];


  gitignore = (pkgs.callPackage (pkgs.fetchFromGitHub
    { owner  = "siers";
      repo   = "nix-gitignore";
      rev    = "4f2d85f2f1aa4c6bff2d9fcfd3caad443f35476e";
      sha256 = "1vzfi3i3fpl8wqs1yq95jzdi6cpaby80n8xwnwa8h2jvcw3j7kdz";
    }) {}).gitignoreSource;


  syd = (import nixpkgs { inherit system; }).fetchFromGitHub {
    owner = "NorfairKing";
    repo = "sydtest";
    rev = "314d53ae175b540817a24d4211dab24fe6cb9232";
    sha256 = "0s40dykqr3d4dcjgw26afiqklspvdbggjywmrikjpx6kp684xfss";
  };


  chill = p: (pkgs.haskell.lib.overrideCabal p {
    inherit enableLibraryProfiling enableExecutableProfiling;
  }).overrideAttrs (_: {
    inherit doHoogle doHaddock strictDeps;
  });


  haskell-overlay = hself: hsuper: {
    mono-traversable = pkgs.haskell.lib.dontCheck hsuper.mono-traversable;
  };


  compactable-overlay = self: super: {
    haskell = super.haskell //
      { packages = super.haskell.packages //
        { ${compiler} = super.haskell.packages.${compiler}.override (old: {
            overrides = super.lib.composeExtensions (old.overrides or (_: _: {})) haskell-overlay;
          });
        };
      };
    };


  pkgs = import nixpkgs {
    inherit system;
    overlays = [
      (import "${syd}/nix/overlay.nix")
      compactable-overlay
    ];
  };


  ghcTools = with pkgs.haskell.packages.${compiler};
    [ cabal-install
      pkgs.stylish-haskell
      ghcid
      easy-hls
    ];


  compactable = pkgs.haskell.packages.${compiler}.callCabal2nix "compactable" (gitignore ignorance ./.) {};


in with pkgs; with lib;

  if asShell
  then pkgs.haskell.packages.${compiler}.shellFor {
    inherit withHoogle;
    packages    = _: [ compactable ];
    COMPILER    = compiler;
    buildInputs = ghcTools;
    shellHook   = ''
      cat ${./intro}
    '';
  } else chill compactable
